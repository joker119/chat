<?php

use EasySwoole\Log\LoggerInterface;

return [
    'SERVER_NAME' => "EasySwoole",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SOCKET_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'reload_async' => true,
            'max_wait_time' => 3,
            'document_root' => EASYSWOOLE_ROOT . '/Public',
            'enable_static_handler' => true,
        ],
        'TASK' => [
            'workerNum' => 4,
            'maxRunningNum' => 128,
            'timeout' => 15
        ]
    ],
    "LOG" => [
        'dir' => null,
        'level' => LoggerInterface::LOG_LEVEL_DEBUG,
        'handler' => null,
        'logConsole' => true,
        'displayConsole' => true,
        'ignoreCategory' => []
    ],
    'TEMP_DIR' => null,
    /*################ MYSQL CONFIG ##################*/
    'MYSQL' => [
        'host'          => '192.168.3.88', // 数据库地址
        'port'          => 3306, // 数据库端口
        'user'          => 'root', // 数据库用户名
        'password'      => 'root', // 数据库用户密码
        'timeout'       => 5, // 数据库连接超时时间
        'charset'       => 'utf8mb4', // 数据库字符编码
        'database'      => 'chat', // 数据库名
        'returnCollection'  => true, // 设置返回结果为 数组
    ],
    /*################ REDIS CONFIG ##################*/
    'REDIS' => [
        'host'          => '192.168.3.88', // Redis 地址
        'port'          => 6379, // Redis 端口
        'auth'          => '', // Redis 密码
    ],
    /*################ FAST_CACHE CONFIG ##################*/
    'FAST_CACHE' => [
        'PROCESS_NUM' => 1, // 进程数,大于0才开启
    ],
];
