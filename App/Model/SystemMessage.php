<?php


namespace App\Model;


class SystemMessage extends Base
{
    protected $tableName = 'system_message';

    public function getTimeAttr($value)
    {
        return date('Y-m-d H:i', $value);
    }
}