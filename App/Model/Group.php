<?php


namespace App\Model;


class Group extends Base
{
    protected $tableName = '`group`';

    public static function getGroup(int $userId): array
    {
        return static::create()
            ->alias('g')
            ->field('g.id,g.groupname,g.avatar')
            ->join('group_member gm', 'g.id = gm.group_id')
            ->where('gm.user_id', $userId)
            ->all()
            ->append('members')
            ->toArray();
    }

    public function getMembersAttr($value, $data): int
    {
        return GroupMember::create()->where('group_id', $data['id'])->count();
    }
}