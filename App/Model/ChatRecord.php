<?php


namespace App\Model;


class ChatRecord extends Base
{
    protected $tableName = 'chat_record';

    public function getTimeStampAttr($value)
    {
        return $value * 1000;
    }
}