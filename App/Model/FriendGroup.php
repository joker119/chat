<?php


namespace App\Model;


class FriendGroup extends Base
{
    protected $tableName = 'friend_group';

    public static function getGroup(int $userId): array
    {
         return static::create()
            ->field('id,groupname,user_id')
            ->where('user_id', $userId)
            ->all()
            ->append('list')
            ->toArray();
    }

    public function getListAttr($value, $data)
    {
        //为何无法联合查询？？？
//        return Friend::create()
//            ->alias('f')
//            ->field('u.id,u.username,u.remark,u.avatar,u.status,f.id as friend_id')
//            ->join('user as u', 'u.id = f.friend_id')
//            ->where('f.friend_group_id', $data['id'])
//            ->where('f.user_id', $data['user_id'])
//            ->order('u.status', 'desc')
//            ->all()
//            ->toArray();
        $result = Friend::create()
            ->field('id,friend_id')
            ->where('friend_group_id', $data['id'])
            ->where('user_id', $data['user_id'])
            ->all()
            ->toArray();
        $userId = array_column($result, 'friend_id');
        if (!$userId) return [];
        return User::create()->field('id,username,remark,avatar,status')->where('id', $userId, 'in')->all()->toArray();
    }
}