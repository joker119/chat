<?php


namespace App\Model;

class User extends Base
{
    protected $tableName = 'user';

    public static function getUserByUsername($username, $field = '*')
    {
        $result = static::create()->field($field)->where('username', $username)->get();
        return $result ? $result->toArray() : $result;
    }

    public function setPasswordAttr($value, $data)
    {
        return password_hash($value, PASSWORD_DEFAULT);
    }

    public static function passwordVerify(string $password, string $uPwd): bool
    {
        return password_verify($password, $uPwd);
    }
}