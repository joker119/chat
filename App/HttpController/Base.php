<?php


namespace App\HttpController;


use EasySwoole\Http\AbstractInterface\Controller;

class Base extends Controller
{
    public function index()
    {
        $this->actionNotFound('index');
    }

    /**
     * 失败返回
     * @param $msg
     * @param string $result
     * @param int $code
     * @return bool
     */
    protected function error($msg, $result = '', $code = 1)
    {
        return $this->writeJson($code, $result, $msg);
    }

    /**
     * 成功返回
     * @param $msg
     * @param $result
     * @param int $code
     * @return bool
     */
    protected function success($msg, $result = '', $code = 0)
    {
        return $this->error($msg, $result, $code);
    }

    /**
     * 没办法要适应一下laymi
     * @param int $statusCode
     * @param null $result
     * @param null $msg
     * @return bool
     */
    protected function writeJson($statusCode = 200, $result = null, $msg = null)
    {
        if (!$this->response()->isEndResponse()) {
            $data = Array(
                "code" => $statusCode,
                "data" => $result,
                "message" => $msg
            );
            $this->response()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type', 'application/json;charset=utf-8');
            $this->response()->withStatus($statusCode);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string|null $action
     */
    protected function actionNotFound(?string $action)
    {
        $this->response()->withStatus(404);
        $file = EASYSWOOLE_ROOT.'/vendor/easyswoole/easyswoole/src/Resource/Http/404.html';
        if(!is_file($file)){
            $file = EASYSWOOLE_ROOT.'/src/Resource/Http/404.html';
        }
        $this->response()->write(file_get_contents($file));
    }
}