<?php


namespace App\HttpController\Api;

use App\HttpController\Base;
use App\Utility\Jwt;
use App\Model\User;
use App\Model\FriendGroup;

/**
 * 用户授权
 * Class Auth
 * @package App\HttpController\Api
 */
class Auth extends Base
{
    /**
     * 登录
     * @Param(name="username",required="",notEmpty="",alias="用户名")
     * @Param(name="password",required="",notEmpty="",alias="密码")
     * @Method(allow={POST})
     */
    public function login()
    {
        $params = $this->request()->getRequestParam();

        $user = User::getUserByUsername($params['username'], 'id, password');
        if (!$user) return $this->error('用户不存在');
        if (!User::passwordVerify($params['password'], $user['password'])) {
            return $this->error('密码错误');
        }

        $token = Jwt::getInstance()->encode(['id' => $user['id']]);

        return $this->success('登录成功', ['token' => $token]);
    }

    /**
     * 注册
     * @Param(name="username",required="",notEmpty="",alias="用户名")
     * @Param(name="password",required="",notEmpty="",alias="密码")
     * @Param(name="nickname",required="",notEmpty="",alias="昵称")
     * @Param(name="avatar",required="",notEmpty="",alias="头像")
     * @Param(name="remark",alias="个人签名")
     * @Method(allow={POST})
     */
    public function register()
    {
        $params = $this->request()->getRequestParam();

        $user = User::getUserByUsername($params['username'], 'id');
        if ($user) return $this->error('用户名已存在');

        $data = [
            'avatar' => $params['avatar'],
            'username' => $params['username'],
            'password' => $params['password'],
            'remark' => $params['remark'],
        ];

        $userId = User::create($data)->save();
        if (!$userId) return $this->error('注册失败');

        FriendGroup::create([
            'user_id' => $userId,
            'groupname' => '默认分组'
        ])->save();

        return $this->success('注册成功');
    }
}