<?php


namespace App\HttpController\Api;


use App\Model\Friend;
use App\Model\Group;
use App\Model\GroupMember;
use App\Model\User;

class Find extends Base
{
    public function find()
    {
        $params = $this->request()->getRequestParam();
        $type = isset($params['type']) ? $params['type'] : 'user';
        $word = isset($params['word']) ? '%' . $params['word'] . '%' : '';
        if(empty($word)) return $this->error('关键词不能为空');

        if($type == 'group'){
            // 剔除自己加过的群
            $group = Group::create();
            $groupId = GroupMember::create()->where('user_id', $this->user['id'])->column('group_id');
            if (!is_null($groupId)) $group->where('id', $groupId, 'not in');
            $list = $group->field('id,groupname,avatar')
                ->where('groupname', $word, 'like')
                ->all()
                ->toArray();
        }else{
            // 剔除自己和加过的好友
            $userId = Friend::create()->where('user_id', $this->user['id'])->column('friend_id');
            $userId = $userId ?? [];
            $userId[] = $this->user['id'];
            $list = User::create()
                ->field('id,username,avatar')
                ->where('username', $word, 'like')
                ->where('id', $userId, 'not in')
                ->all()
                ->toArray();
        }
        return $this->success('获取搜索列表', $list);
    }
}