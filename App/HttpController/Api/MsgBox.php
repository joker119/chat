<?php


namespace App\HttpController\Api;


use App\Model\SystemMessage;
use App\Model\User;

class MsgBox extends Base
{
    /**
     * 消息盒子
     */
    public function msgBox()
    {
        SystemMessage::create()->where('user_id', $this->user['id'])->update(['read' => 1]);
        // 这联表查询无语了，查不出
//        $systemMessage = SystemMessage::create()
//            ->alias('sm')
//            ->field('sm.*,u.avatar,u.username')
//            ->join('user u', 'u.id = sm.from_id')
//            ->where('sm.user_id', $this->user['id'])
//            ->order('sm.id', 'desc')
//            ->all()
//            ->toArray();
        $systemMessage = SystemMessage::create()
            ->where('user_id', $this->user['id'])
            ->order('id', 'desc')
            ->all()
            ->toArray();
        $userId = array_unique(array_column($systemMessage, 'user_id'));
        $user = $userId ? User::create()->field('id,avatar,username')->where('id', $userId, 'in')->indexBy('id') : [];
        foreach ($systemMessage as &$item) {
            if(isset($user[$item['user_id']])) {
                unset($user[$item['user_id']]['id']);
                $item += $user[$item['user_id']];
            }
        }
        unset($item);
        return $this->success('获取消息', $systemMessage);
    }
}