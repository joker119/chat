<?php


namespace App\HttpController\Api;


use App\Model\Group;
use App\Model\FriendGroup;

/**
 * 用户
 * Class User
 * @package App\HttpController\Api
 */
class User extends Base
{
    public function info()
    {
        $data = [
            'mine' => $this->user,
            'friend' => FriendGroup::getGroup($this->user['id']),
            'group' => Group::getGroup($this->user['id']),
        ];
        return $this->success('获取个人信息', $data);
    }
}