<?php


namespace App\HttpController\Api;


use App\Model\SystemMessage;
use App\Model\Friend as FriendModel;
use App\Model\User;
use EasySwoole\ORM\DbManager;
use Throwable;

class Friend extends Base
{
    public function agree()
    {
        $params = $this->request()->getRequestParam();
        $id = $params['id'];
        $systemMessage = SystemMessage::create()->where('id', $id)->get();
        $isFriend = FriendModel::create()->where('user_id', $systemMessage['user_id'])->where('friend_id', $systemMessage['from_id'])->get();
        if ($isFriend) {
            return $this->error('已添加');
        }

        $data = [
            [
                'user_id' => $systemMessage['user_id'],
                'friend_id' => $systemMessage['from_id'],
                'friend_group_id' => $params['groupid']
            ],
            [
                'user_id' => $systemMessage['from_id'],
                'friend_id' => $systemMessage['user_id'],
                'friend_group_id' => $systemMessage['group_id']
            ]
        ];
        $res = FriendModel::create()->saveAll($data);
        if (!$res) {
            return $this->error('添加失败');
        }

        SystemMessage::create()->where('id', $id)->update(['status' => 1]);
        SystemMessage::create([
            'user_id' => $systemMessage['from_id'],
            'from_id' => $systemMessage['user_id'],
            'type' => 1,
            'status' => 1,
            'time' => time()
        ])->save();
        $user = User::create()->where('id', $systemMessage['from_id'])->get();
        $data = [
            "type" => "friend",
            "avatar" => $user['avatar'],
            "username" => $user['username'],
            "groupid" => $params['groupid'],
            "id" => $user['id'],
            "remark" => $user['remark']
        ];
        return $this->success('添加成功', $data);
    }

    public function refuse()
    {
        $params = $this->request()->getRequestParam();
        $id = $params['id'];
        $systemMessage = SystemMessage::create()->where('id', $id)->get();
        try{
            DbManager::getInstance()->startTransaction();
            SystemMessage::create()->where('id', $id)->update(['status' => 2]);
            SystemMessage::create([
                'user_id' => $systemMessage['from_id'],
                'from_id' => $systemMessage['user_id'],
                'type' => 1,
                'status' => 2,
                'time' => time()
            ])->save();
            DbManager::getInstance()->commit();
        }catch (Throwable $e){
            DbManager::getInstance()->rollback();
            return $this->error('拒绝失败');
        }
        return $this->success('拒绝成功');
    }
}