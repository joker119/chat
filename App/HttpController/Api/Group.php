<?php


namespace App\HttpController\Api;


use App\Model\Group as GroupModel;
use App\Model\GroupMember;
use App\Model\User;
use EasySwoole\ORM\DbManager;
use Throwable;

class Group extends Base
{
    public function create()
    {
        $params = $this->request()->getRequestParam();
        $data = [
            'groupname' => $params['groupname'],
            'user_id' => $this->user['id'],
            'avatar' => $params['avatar']
        ];
        try{
            DbManager::getInstance()->startTransaction();
            $groupId = GroupModel::create($data)->save();
            GroupMember::create([
                'group_id' => $groupId,
                'user_id' => $this->user['id'],
            ])->save();
            $data['id'] = $groupId;
            DbManager::getInstance()->commit();
        } catch (Throwable $e) {
            DbManager::getInstance()->rollback();
            return $this->error('创建失败：' . $e->getMessage());
        }
        return $this->success('创建成功', $data);
    }

    public function join()
    {
        $params = $this->request()->getRequestParam();
        $id = $params['groupid'];

        $group = GroupModel::create()->get($id);
        if (!$group) return $this->error('群不存在');

        $groupMember = GroupMember::create()->field('id')->where('group_id', $group['id'])->where('user_id', $this->user['id'])->get();
        if ($groupMember) return $this->error("已经加入");

        $result = GroupMember::create()->data(['group_id' => $id, 'user_id' => $this->user['id']])->save();
        if (!$result) return $this->error("加入失败");
        return $this->success("加入成功", $group);
    }

    /**
     * 获取群成员
     */
    public function members()
    {
        $params = $this->request()->getRequestParam();
        $id = $params['id'];
//        为何联表查询不出来，orm没写好？
//        $groupMember = GroupMember::create()
//            ->alias('gm')
//            ->field('u.username,u.id,u.avatar,u.remark')
//            ->join('user as u', 'u.id=gm.user_id')
//            ->where('gm.group_id', $id)
//            ->all()
//            ->toArray();
        $groupMember = GroupMember::create()->where('group_id', $id)->column('user_id');
        if(!$groupMember) return $this->success('获取群成员', []);
        $user = User::create()->field('id,username,avatar,remark')->where('id', $groupMember, 'in')->all()->toArray();
        return $this->success('获取群成员', ['list' => $user]);
    }
}