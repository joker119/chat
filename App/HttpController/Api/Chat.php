<?php


namespace App\HttpController\Api;


use App\Model\ChatRecord;
use App\Model\User;

class Chat extends Base
{
    /**
     * 聊天记录
     */
    public function log()
    {
        $params = $this->request()->getRequestParam();
        $id = $params['id'];
        $type = $params['type'];
        $page = $params['page'];
        $limit = $params['limit'] ?? 10;
        // 为何联表查询查不出？？？？？？？？？
//        $model = ChatRecord::create()
//            ->alias('cr')
//            ->field('u.id,u.username,u.avatar,cr.time as timestamp,cr.content')
//            ->join('user as u', 'u.id = cr.user_id');
        $model = ChatRecord::create()
            ->alias('cr')
            ->field('cr.user_id,cr.time,cr.content');
        if ($type == 'group') {
            $model->where('cr.group_id', $id);
        } else {
            $model->where('cr.user_id', $this->user['id'])
                ->where('cr.friend_id', $id)
                ->where('cr.user_id', $id, '=', 'or')
                ->where('cr.friend_id', $this->user['id']);
        }
        $model->order('cr.time', 'desc')
            ->limit($limit * ($page - 1), $limit)
            ->withTotalCount();
        $chatRecord = $model->all()->toArray();
        // 联表查询查不出要作处理开始
        $userId = array_unique(array_column($chatRecord, 'user_id'));
        $user = $userId ? User::create()->field('id,username,avatar')->where('id', $userId, 'in')->indexBy('id') : [];
        foreach ($chatRecord as &$item){
            $item['timestamp'] = $item['time'] * 1000;
            if(isset($user[$item['user_id']])) {
                $item += $user[$item['user_id']];
            }
            unset($item['time']);
            unset($item['user_id']);
        }
        unset($item);
        // 联表查询查不出要作处理结束
        $result['data'] = $chatRecord;
        $result['last_page'] = ceil($model->lastQueryResult()->getTotalCount() / $limit);
        return $this->success('获取聊天记录', $result);
    }
}