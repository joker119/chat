<?php


namespace App\HttpController;


use EasySwoole\Http\Message\UploadFile;

class Index extends Base
{
    /**
     * 上传图片
     */
    public function upload()
    {
        $img = $this->request()->getUploadedFile('file');
        if (!$img) $this->error('请选择上传的文件');
        if ($img->getSize() > 1024 * 1024 * 5) $this->error('图片不能大于5M');

        $MediaType = explode("/", $img->getClientMediaType());
        $MediaType = $MediaType[1] ?? "";
        if (!in_array($MediaType, ['png', 'jpg', 'gif', 'jpeg', 'pem', 'ico'])) {
            $this->error('文件类型不正确！');
        }

        $path = '/upload/';
        $dir = EASYSWOOLE_ROOT . '/Public' . $path;
        $fileName = uniqid() . $img->getClientFileName();
        if (!is_dir($dir)) mkdir($dir, 0777, true);
        try {
            $img->moveTo($dir . $fileName);
            $data = [
                'name' => $fileName,
                'src' => $path . $fileName,
            ];
            $this->success('上传成功', $data);
        } catch (\Throwable $throwable) {
            $this->error('上传失败：' . $throwable->getMessage());
        }
    }
}