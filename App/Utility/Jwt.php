<?php


namespace App\Utility;

use EasySwoole\Component\Singleton;
use EasySwoole\Jwt\Jwt as Jwt2;
use EasySwoole\Jwt\Exception;

class Jwt
{
    use Singleton;

    private $key = 'easyswoole';

    public function encode(array $data): string
    {
        $jwt = Jwt2::getInstance()
            ->setSecretKey($this->key) // 秘钥
            ->publish();
        $jwt->setAlg('HMACSHA256'); // 加密方式
        $jwt->setAud('user'); // 用户
        $jwt->setExp(time()+3600); // 过期时间
        $jwt->setIat(time()); // 发布时间
        $jwt->setIss($this->key); // 发行人
        $jwt->setJti(md5(time())); // jwt id 用于标识该jwt
        $jwt->setNbf(time()+60*5); // 在此之前不可用
        $jwt->setSub('主题'); // 主题
        // 自定义数据
        $jwt->setData($data);
        return $jwt->__toString();
    }

    public function decode(string $token)
    {
        try {
            $jwt = Jwt2::getInstance()->setSecretKey($this->key)->decode($token);
            $status = $jwt->getStatus();
            switch ($status)
            {
                case 1:
                    return $jwt->getData();
                case  -1:
                    return 'token无效';
                case  -2:
                    return 'token过期';
            }
        } catch (Exception $e) {
            return '非法token';
        }
    }
}