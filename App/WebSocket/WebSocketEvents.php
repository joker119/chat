<?php


namespace App\WebSocket;


use App\Model\Friend;
use App\Model\OfflineMessage;
use App\Model\SystemMessage;
use EasySwoole\FastCache\Cache;
use App\Model\User;
use App\Utility\Jwt;

class WebSocketEvents
{
    /**
     * @param \Swoole\WebSocket\Server $server
     * @param \Swoole\Http\Request $request
     */
    public static function onOpen(\Swoole\WebSocket\Server $server, \Swoole\Http\Request $request)
    {
        // token验证
        $token = $request->get['token'];
        if (!isset($token) || empty($token)) $msg = '没有token';
        $user = Jwt::getInstance()->decode($token);
        if (is_string($user)) $msg = $user;
        $user = User::create()->field('id')->get($user['id']);
        if (!$user) $msg = '用户不存在';
        if (!empty($msg)) {
            $server->push($request->fd, json_encode(['action' => WebSocketAction::TOKEN, 'msg' => $msg]));
            return;
        }

        // 绑定fd变更状态
        Cache::getInstance()->set('uid' . $user['id'], ["value" => $request->fd], 3600);
        Cache::getInstance()->set('fd' . $request->fd, ["value" => $user['id']], 3600);
        User::create()->where('id', $user['id'])->update(['status' => 'online']);// 标记为在线

        //通知好友上线
        static::status($server, $user['id']);
        //获取未读消息盒子数量
        $data = [
            'action' => WebSocketAction::MSG_BOX,
            'count' => SystemMessage::create()->where('user_id', $user['id'])->where('read', 0)->count(),
        ];
        //检查离线消息
        $offlineMessage = OfflineMessage::create()->where('user_id', $user['id'])->where('status', 0)->all()->toArray();
        foreach ($offlineMessage as $item) {
            $fd = Cache::getInstance()->get('uid' . $user['id']);//获取接受者fd
            if ($fd) {
                $server->push($fd['value'], $item['data']);//发送消息
                OfflineMessage::create()->where('id', $item['id'])->update(['status' => 1]);
            }
        }
        $server->push($request->fd, json_encode($data));
    }

    /**
     * 链接被关闭时
     * @param \Swoole\Server $server
     * @param int $fd
     * @param int $reactorId
     * @throws Exception
     */
    public static function onClose(\Swoole\Server $server, int $fd, int $reactorId)
    {
        $userId = Cache::getInstance()->get('fd' . $fd);
        if (!is_null($userId)) {
            static::status($server, $userId['value'], 'offline');
            Cache::getInstance()->unset('uid' . $userId['value']);// 解绑uid
            User::create()->where('id', $userId['value'])->update(['status' => 'offline']);
        }
        Cache::getInstance()->unset('fd' . $fd);// 解绑fd
    }

    /**
     * 通知好友，我的状态
     * @param $server
     * @param $userId
     * @param string $status
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    private static function status($server, $userId, $status = 'online')
    {
        $friend = Friend::create()->where('user_id', $userId)->column('friend_id');
        $data = [
            "action" => WebSocketAction::FRIEND_STATUS,
            "uid" => $userId,
            "status" => $status,
        ];
        if($friend) {
            foreach ($friend as $item) {
                $fd = Cache::getInstance()->get('uid' . $item);//获取接受者fd
                if ($fd) {
                    $server->push($fd['value'], json_encode($data));//发送消息
                }
            }
        }
    }
}