<?php


namespace App\WebSocket;


use EasySwoole\Socket\AbstractInterface\ParserInterface;
use EasySwoole\Socket\Bean\Caller;
use EasySwoole\Socket\Bean\Response;
use EasySwoole\Socket\Client\WebSocket as WebSocketClient;

class WebSocketParser implements ParserInterface
{
    /**
     * 解码上来的消息
     * @param string $raw 消息内容
     * @param WebSocketClient $client 当前的客户端
     * @return Caller|null
     */
    public function decode($raw, $client): ?Caller
    {
        $caller = new Caller();
        if ($raw !== 'PING') {
            $data = json_decode($raw, true);
            $controller = !empty($data['controller']) ? ucfirst($data['controller']) : 'Index';
            $action = !empty($data['action']) ? $data['action'] : 'index';
            $param = !empty($data['param']) ? $data['param'] : [];
            $controller = "App\\WebSocket\\Controller\\{$controller}";
            $caller->setControllerClass($controller);
            $caller->setAction($action);
            $caller->setArgs($param);
        } else {
            $caller->setControllerClass('\\App\\WebSocket\\Controller\\Index');
            $caller->setAction('heartbeat');
        }
        return $caller;
    }

    /**
     * 打包下发的消息
     * @param Response $response 控制器返回的响应
     * @param WebSocketClient $client 当前的客户端
     * @return string|null
     */
    public function encode(Response $response, $client): ?string
    {
        $message = $response->getMessage();
        return is_array($message) ? json_encode($message) : $message;
    }
}