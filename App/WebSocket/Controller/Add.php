<?php


namespace App\WebSocket\Controller;


use App\Model\Friend;
use App\Model\FriendGroup;
use App\Model\Group;
use App\Model\GroupMember;
use App\Model\OfflineMessageModel;
use App\Model\SystemMessage;
use App\Model\User;
use App\WebSocket\WebSocketAction;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\FastCache\Cache;
use EasySwoole\ORM\DbManager;
use Throwable;

class Add extends Base
{
    public function friend()
    {
        $param = $this->caller()->getArgs();
        $friendId = $param['friend_id'] ?? 0;
        $friendGroupId = $param['friend_group_id'] ?? 0;
        $remark = $param['remark'] ?? '';
        if (empty($friendId)) return $this->error('没有用户ID');
        if (empty($friendGroupId)) return $this->error('没有好友分组ID');
        // 用户是否存在
        $friend = User::create()->field('id')->get($friendId);
        if (!$friend) return $this->error('没有此用户');
        // 不能添加自己
        $user = $this->currentUser();
        if ($friendId == $user['id']) return $this->error('不能添加自己');
        // 分组是否存在
        $friendGroup = FriendGroup::create()->field('id')->get(['id' => $friendGroupId, 'user_id' => $user['id']]);
        if (!$friendGroup) return $this->error('没有此分组');
        // 是否已加
        $hasFriend = Friend::create()->field('id')->get(['user_id' => $user['id'], 'friend_id' => $friend['id']]);
        if ($hasFriend) return $this->error('已添加过好友');

        try {
            DbManager::getInstance()->startTransaction();
            SystemMessage::create([
                'user_id' => $friend['id'],//接受者
                'from_id' => $user['id'],//来源者
                'group_id' => $friendGroup['id'],
                'remark' => $remark,
                'time' => time(),
            ])->save();
            // 消息盒子提醒
            $data = [
                'action' => WebSocketAction::MSG_BOX,
                'count' => SystemMessage::create()->where('user_id', $friend['id'])->where('read', 0)->count(),
            ];
            // 好友在线就推送给给它，不在线就插入离线消息
            $fd = Cache::getInstance()->get('uid' . $friend['id']);
            if (is_null($fd)) {
                //获取好友未读消息数量
                OfflineMessageModel::create([
                    'user_id' => $friend['id'],
                    'data' => json_encode($data),
                ])->save();
            } else {
                $server = ServerManager::getInstance()->getSwooleServer();
                $server->push($fd['value'], json_encode($data));
            }
            DbManager::getInstance()->commit();
        } catch(Throwable  $e){
            // 回滚事务
            DbManager::getInstance()->rollback();
        }
        return true;
    }

    public function agree()
    {
        $param = $this->caller()->getArgs();
        $user = $this->currentUser();
        $data = [
            "action" => WebSocketAction::ADD_LIST,
            "data" => [
                "type" => "friend",
                "avatar" => $user['avatar'],
                "username" => $user['username'],
                "groupid" => $param['fromgroup'],
                "id" => $user['id'],
                "remark" => $user['remark']
            ]
        ];
        //获取未读消息盒子数量
        $count = SystemMessage::create()->where('user_id', $param['id'])->where('read', 0)->count();

        $data1 = [
            "action" => WebSocketAction::MSG_BOX,
            "count" => $count,
        ];

        $fd = Cache::getInstance()->get('uid' . $param['id']);//获取接受者fd
        if ($fd == false) {
            //这里说明该用户已下线，日后做离线消息用
            $offline_message = [
                'user_id' => $param['id'],
                'data' => json_encode($data1),
            ];
            //插入离线消息
            OfflineMessageModel::create($offline_message)->save();
        } else {
            $server = ServerManager::getInstance()->getSwooleServer();
            $server->push($fd['value'], json_encode($data));//发送消息
            $server->push($fd['value'], json_encode($data1));//发送消息
        }
    }

    public function refuse()
    {
        $param = $this->caller()->getArgs();
        $id = $param['id'] ?? 0;//消息id
        if (empty($id)) return $this->error('没有消息ID');
        $systemMessage = SystemMessage::create()->field('from_id')->get($id);
        if(!$systemMessage) return $this->error('消息不存在');

        $data = [
            "action" => "msgBox",
            "count" => SystemMessage::create()->where('user_id', $systemMessage['from_id'])->where('read', 0)->count(),
        ];
        $fd = Cache::getInstance()->get('uid' . $systemMessage['from_id']);
        if ($fd) {
            $server = ServerManager::getInstance()->getSwooleServer();
            $server->push($fd['value'], json_encode($data));//发送消息
        }
        return true;
    }

    public function groupNotify()
    {
        $param = $this->caller()->getArgs();
        $groupid = $param['groupid'] ?? 0;
        if (empty($groupid)) return $this->error('没有群聊ID');
        $user = $this->currentUser();
        $groupMember = GroupMember::create()->where('group_id', $groupid)->where('user_id', $user['id'], '<>')->all()->toArray();

        $group = Group::create()->field('groupname')->get($groupid);
        $data = [
            'action' => WebSocketAction::GROUP_NOTIFY,
            'data' => [
                'id' => $groupid,
                'content' => $user['username'] . '加入了 ' . $group['groupname'] . '群'
            ]
        ];
        // 异步推送
        TaskManager::getInstance()->async(function () use ($groupMember, $user, $data) {
            $server = ServerManager::getInstance()->getSwooleServer();
            foreach ($groupMember as $item) {
                $fd = Cache::getInstance()->get('uid' . $item['user_id']);//获取fd
                if (!is_null($fd)) {
                    $server->push($fd['value'], json_encode($data));//发送消息
                }
            }
        });
        return true;
    }
}