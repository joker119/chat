<?php


namespace App\WebSocket\Controller;


use App\Model\ChatRecord;
use App\Model\GroupMember;
use App\Model\OfflineMessage;
use App\WebSocket\Action\User\UserInfo;
use App\WebSocket\WebSocketAction;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\FastCache\Cache;

class Index extends Base
{
    /**
     * 心跳回应
     */
    public function heartbeat()
    {
        $this->response()->setMessage('PONG');
    }

    public function chat()
    {
        $param = $this->caller()->getArgs();
        $user = $this->currentUser();
        $isFriend = $param['to']['type'] === 'friend';
        $data = [
            'username' => $param['mine']['username'],
            'avatar' => $param['mine']['avatar'],
            'id' => $isFriend ? $param['mine']['id'] :$param['to']['id'],
            'type' => $param['to']['type'],
            'content' => $param['mine']['content'],
            'cid' => 0,
            'mine' => false,//要通过判断是否是我自己发的
            'fromid' => $param['mine']['id'],
            'timestamp' => time() * 1000,
            'action' => $isFriend ? WebSocketAction::FRIEND : WebSocketAction::GROUP
        ];
        if ($param['to']['type'] == "friend") {
            // 私聊
            if ($user['id'] == $param['to']['id']) {
                return $this->error('不能给自己发消息');
            }
            $fd = Cache::getInstance()->get('uid' . $param['to']['id']);//获取接受者fd
            if (is_null($fd)) {
                //这里说明该用户已下线，日后做离线消息用
                $offlineMessage = [
                    'user_id' => $param['to']['id'],
                    'data' => json_encode($data),
                ];
                //插入离线消息
                OfflineMessage::create($offlineMessage)->save();
            } else {
                $server = ServerManager::getInstance()->getSwooleServer();
                $server->push($fd['value'], json_encode($data));//发送消息
            }
        } else {
            //群消息
            $groupMember = GroupMember::create()
                ->field('user_id')
                ->where('group_id', $param['to']['id'])
                ->all()
                ->toArray();

            // 异步推送
            TaskManager::getInstance()->async(function () use ($groupMember, $user, $data) {
                $server = ServerManager::getInstance()->getSwooleServer();
                foreach ($groupMember as $item) {
                    if ($item['user_id'] == $user['id']) {
                        continue;
                    }
                    $fd = Cache::getInstance()->get('uid' . $item['user_id']);//获取接受者fd
                    if (is_null($fd)) {
                        //这里说明该用户已下线，日后做离线消息用
                        $offlineMessage = [
                            'user_id' => $item['user_id'],
                            'data' => json_encode($data),
                        ];
                        //插入离线消息
                        OfflineMessageModel::create($offlineMessage)->save();
                    } else {
                        $server->push($fd['value'], json_encode($data));//发送消息
                    }
                }
            });
        }
        //记录聊天记录
        $recordData = [
            'user_id' => $param['mine']['id'],
            'friend_id' => $isFriend ? $param['to']['id'] : 0,
            'group_id' => !$isFriend ? $param['to']['id'] : 0,
            'content' => $param['mine']['content'],
            'time' => time()
        ];
        ChatRecord::create($recordData)->save();
        return true;
    }
}