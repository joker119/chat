<?php

namespace App\WebSocket\Controller;


use App\Model\User;
use App\WebSocket\WebSocketAction;
use EasySwoole\FastCache\Cache;
use EasySwoole\Socket\AbstractInterface\Controller;

/**
 * 基础控制器
 * Class Base
 * @package App\WebSocket\Controller
 */
class Base extends Controller
{
    /**
     * 获取当前的用户
     * @return array|string
     * @throws Exception
     */
    protected function currentUser()
    {
        /** @var \EasySwoole\Socket\Client\WebSocket $client */
        $client = $this->caller()->getClient();
        $data = Cache::getInstance()->get('fd' . $client->getFd());
        if(is_null($data)) $this->response()->setMessage(['type' => WebSocketAction::TOKEN, 'msg' => 'token过期']);
        return User::create()->get($data['value']);
    }

    protected function error($msg = '', $data = '', $type = WebSocketAction::MSG, $code = 1): bool
    {
        $this->response()->setMessage(['action' => $type, 'code' => $code, 'msg' => $msg, 'data' => $data]);
        return false;
    }

    protected function success($type, $msg = '', $data = '', $code = 0): bool
    {
        return $this->error($msg, $data, $type, $code);
    }
}
