<?php


namespace App\WebSocket;


class WebSocketAction
{
    const TOKEN = 'token'; // TOKEN

    const MSG = 'msg'; // 显示错误

    const FRIEND_STATUS = 'friendStatus'; // 好友状态

    const ADD_LIST = 'addList'; // 添加好友

    const GROUP_NOTIFY = 'groupNotify'; // 加入群通知

    const MSG_BOX= 'msgBox'; // 消息盒子

    const FRIEND = 'friend'; // 私聊消息

    const GROUP = 'group'; // 群聊消息
}