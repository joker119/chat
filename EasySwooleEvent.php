<?php


namespace EasySwoole\EasySwoole;


use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\Socket\Dispatcher;
use App\WebSocket\WebSocketParser;
use App\WebSocket\WebSocketEvents;
use EasySwoole\FileWatcher\FileWatcher;
use EasySwoole\FileWatcher\WatchRule;
use EasySwoole\FastCache\Cache;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');

        ###### 注册 mysql orm 连接池 ######
        $dbConfig = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        $dbConfig->setMaxObjectNum(20)->setMinObjectNum(5);
        DbManager::getInstance()->addConnection(new Connection($dbConfig));

        ###### 注册 redis 连接池 ######
        $redisConfig = new \EasySwoole\Redis\Config\RedisConfig();
        \EasySwoole\RedisPool\RedisPool::getInstance()->register($redisConfig);
    }

    public static function mainServerCreate(EventRegister $register)
    {
        /**
         * **************** websocket控制器 **********************
         */
        // 创建一个 Dispatcher 配置
        $config = new \EasySwoole\Socket\Config();
        $config->setType($config::WEB_SOCKET);
        $config->setParser(WebSocketParser::class);
        $dispatch = new Dispatcher($config);
        // 给server 注册相关事件 在 WebSocket 模式下  message 事件必须注册 并且交给 Dispatcher 对象处理
        $register->set($register::onMessage, function (\Swoole\Websocket\Server $server, \Swoole\WebSocket\Frame $frame) use ($dispatch) {
            $dispatch->dispatch($server, $frame->data, $frame);
        });
        $register->set($register::onOpen, [WebSocketEvents::class, 'onOpen']);
        $register->set($register::onClose, [WebSocketEvents::class, 'onClose']);

        /**
         * ****************   服务热重启    ****************
         */
        $fileWatcher = new FileWatcher;
        $rule = new WatchRule(EASYSWOOLE_ROOT . "/App"); // 设置监控规则和监控目录
        $fileWatcher->addRule($rule);
        $fileWatcher->setOnChange(function () {
            Logger::getInstance()->info('file change ,reload!!!');
            ServerManager::getInstance()->getSwooleServer()->reload();
        });
        $fileWatcher->attachServer(ServerManager::getInstance()->getSwooleServer());

        /**
         * ****************   缓存服务    ****************
         */
        $fastCacheConfig = new \EasySwoole\FastCache\Config();
        $fastCacheConfig->setTempDir(EASYSWOOLE_TEMP_DIR);
        Cache::getInstance($fastCacheConfig)->attachToServer(ServerManager::getInstance()->getSwooleServer());
    }
}