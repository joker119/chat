/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : chat

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 29/01/2022 14:44:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat_record
-- ----------------------------
DROP TABLE IF EXISTS `chat_record`;
CREATE TABLE `chat_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL DEFAULT 0 COMMENT '是群聊消息记录的话 此id为0',
  `group_id` int(11) NOT NULL DEFAULT 0 COMMENT '如果不为0说明是群聊',
  `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of chat_record
-- ----------------------------
INSERT INTO `chat_record` VALUES (1, 1, 3, 0, 'aaa', 1643436391);
INSERT INTO `chat_record` VALUES (2, 3, 1, 0, '?\n', 1643436397);
INSERT INTO `chat_record` VALUES (3, 3, 1, 0, '[pre class=layui-code]<p>asdM</p>[/pre]', 1643436414);
INSERT INTO `chat_record` VALUES (4, 1, 3, 0, 'face[哈哈] ', 1643436417);
INSERT INTO `chat_record` VALUES (5, 3, 1, 0, 'img[/upload/61f4d986700995d09ceac4de6312262La2-0.jpg]', 1643436422);
INSERT INTO `chat_record` VALUES (6, 1, 0, 1, '123123', 1643436499);
INSERT INTO `chat_record` VALUES (7, 2, 0, 1, '???', 1643436502);
INSERT INTO `chat_record` VALUES (8, 2, 0, 1, '111', 1643436527);
INSERT INTO `chat_record` VALUES (9, 3, 0, 1, 'asdasd', 1643436530);
INSERT INTO `chat_record` VALUES (10, 3, 0, 1, 'face[生病] ', 1643436533);
INSERT INTO `chat_record` VALUES (11, 1, 3, 0, '123123', 1643438159);
INSERT INTO `chat_record` VALUES (12, 1, 3, 0, 'asddas', 1643438181);
INSERT INTO `chat_record` VALUES (13, 1, 3, 0, 'asd', 1643438182);
INSERT INTO `chat_record` VALUES (14, 1, 3, 0, '111', 1643438247);
INSERT INTO `chat_record` VALUES (15, 1, 3, 0, 'face[太开心] ', 1643438249);
INSERT INTO `chat_record` VALUES (16, 1, 3, 0, 'face[汗] ', 1643438252);

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `friend_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '好友表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES (1, 3, 1, 3);
INSERT INTO `friend` VALUES (2, 1, 3, 1);
INSERT INTO `friend` VALUES (3, 2, 1, 2);
INSERT INTO `friend` VALUES (4, 1, 2, 1);

-- ----------------------------
-- Table structure for friend_group
-- ----------------------------
DROP TABLE IF EXISTS `friend_group`;
CREATE TABLE `friend_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `groupname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of friend_group
-- ----------------------------
INSERT INTO `friend_group` VALUES (1, 1, '默认分组');
INSERT INTO `friend_group` VALUES (2, 2, '默认分组');
INSERT INTO `friend_group` VALUES (3, 3, '默认分组');

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '群组所属用户id,群主',
  `groupname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '群组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (1, 2, '群111', '/upload/61f4d9ab3e2bb5d09c8d7d3145timg.jpg');

-- ----------------------------
-- Table structure for group_member
-- ----------------------------
DROP TABLE IF EXISTS `group_member`;
CREATE TABLE `group_member`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of group_member
-- ----------------------------
INSERT INTO `group_member` VALUES (1, 1, 2);
INSERT INTO `group_member` VALUES (2, 1, 1);
INSERT INTO `group_member` VALUES (3, 1, 3);

-- ----------------------------
-- Table structure for offline_message
-- ----------------------------
DROP TABLE IF EXISTS `offline_message`;
CREATE TABLE `offline_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未发送 1已发送',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '离线消息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of offline_message
-- ----------------------------
INSERT INTO `offline_message` VALUES (1, 3, '{\"username\":\"test\",\"avatar\":\"\\/upload\\/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":1,\"type\":\"friend\",\"content\":\"asddas\",\"cid\":0,\"mine\":false,\"fromid\":1,\"timestamp\":1643438181000,\"action\":\"friend\"}', 1);
INSERT INTO `offline_message` VALUES (2, 3, '{\"username\":\"test\",\"avatar\":\"\\/upload\\/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":1,\"type\":\"friend\",\"content\":\"asd\",\"cid\":0,\"mine\":false,\"fromid\":1,\"timestamp\":1643438182000,\"action\":\"friend\"}', 1);
INSERT INTO `offline_message` VALUES (3, 3, '{\"username\":\"test\",\"avatar\":\"\\/upload\\/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":1,\"type\":\"friend\",\"content\":\"111\",\"cid\":0,\"mine\":false,\"fromid\":1,\"timestamp\":1643438247000,\"action\":\"friend\"}', 1);
INSERT INTO `offline_message` VALUES (4, 3, '{\"username\":\"test\",\"avatar\":\"\\/upload\\/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":1,\"type\":\"friend\",\"content\":\"face[\\u592a\\u5f00\\u5fc3] \",\"cid\":0,\"mine\":false,\"fromid\":1,\"timestamp\":1643438249000,\"action\":\"friend\"}', 1);
INSERT INTO `offline_message` VALUES (5, 3, '{\"username\":\"test\",\"avatar\":\"\\/upload\\/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":1,\"type\":\"friend\",\"content\":\"face[\\u6c57] \",\"cid\":0,\"mine\":false,\"fromid\":1,\"timestamp\":1643438252000,\"action\":\"friend\"}', 1);

-- ----------------------------
-- Table structure for system_message
-- ----------------------------
DROP TABLE IF EXISTS `system_message`;
CREATE TABLE `system_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '接收用户id',
  `from_id` int(11) NOT NULL COMMENT '来源相关用户id',
  `group_id` int(11) NOT NULL DEFAULT 0,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '添加好友附言',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0好友请求 1请求结果通知',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未处理 1同意 2拒绝',
  `read` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未读 1已读，用来显示消息盒子数量',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统消息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_message
-- ----------------------------
INSERT INTO `system_message` VALUES (1, 3, 1, 1, '111111111111111', 0, 1, 1, 1643436371);
INSERT INTO `system_message` VALUES (2, 1, 3, 0, '', 1, 1, 1, 1643436379);
INSERT INTO `system_message` VALUES (3, 2, 1, 1, '34234', 0, 1, 1, 1643438292);
INSERT INTO `system_message` VALUES (4, 1, 2, 0, '', 1, 1, 1, 1643438299);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '头像',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '签名',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'online' COMMENT 'online在线 hide隐身 offline离线',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '/upload/61f4d92070c475d09c6a5b6def15337177846531748ac16fb.jpg', 'test', '$2y$10$BKHCF5.CkUzpGCSwEp6vm.qjX/YewQLqS9MOIhRzgE8eFqoKSKeiO', '111', 'online');
INSERT INTO `user` VALUES (2, '/upload/61f4d92b235f05d09c7da7bc97tx20218.jpg', 'test2', '$2y$10$2yJLt8HzFBc0CALSNNJBAO7AeOe8CWgTe.VHGMjOWHSDK4Ge0yOwq', '2222', 'offline');
INSERT INTO `user` VALUES (3, '/upload/61f4d93808fa15d09cdca1dffb3-1G123203S6-50.jpg', 'test3', '$2y$10$GOl8TV.Gp3jF5MmQhgImZOawgPob9C8xyMUZ/CSszGNnOfoBCUCKa', '333', 'offline');

SET FOREIGN_KEY_CHECKS = 1;
