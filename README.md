# 微聊

EASYSWOOLE 聊天室

## 安装

安装时遇到提示是否覆盖,全部为`N`.

```bash
git clone https://github.com/easy-swoole/demo.git
cd demo
git checkout 3.x-chat
composer install
php vendor/easyswoole/easyswoole/bin/easyswoole install
composer dump-autoload -o
```

## 配置

修改 `dev.php` 内的配置项，改为自己服务器的信息

```ini
'HOST' => 'http://127.0.0.1:9501',
'WEBSOCKET_HOST' => 'ws://127.0.0.1:9501',
```

## 启动

```bash
php easyswoole server start
```